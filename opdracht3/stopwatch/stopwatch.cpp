/*
 * MIC Opdracht 3
 * Nick van Ravenzwaaij
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <Wire.h>

//functie declaraties
void show_time(uint8_t time);

//constanten en variabelen
#define ZERO 0 //standaard waarde van de counter
#define TRUE 1 //'booleans' gebruiken zonder magic numbers
#define FALSE 0 //'booleans' gebruiken zonder magic numbers
#define COUNTERMAX 9 //waarde voor het contorleren van de counter overflow
#define OUTPUTCOMPAREMAX 54000 //aantal stappen voor de counter tot het 0.864 sec heeft bereikt
uint8_t display[] //array voor binaire waardes die naar de port-expander worden gestuurd
{
	0b01110111, //0
	0b00010100, //1
	0b10110011, //2
	0b10110110, //3
	0b11010100, //4
	0b11100110, //5
	0b11100111, //6
	0b00110100, //7
	0b11110111, //8
	0b11110110, //9
};
volatile uint8_t counter = 0; //counter voor het tellen van centibeats, standaardwaarde 0
volatile int show_time_flag = FALSE; //flag voor het laten zien van time

int main(void) //main functie
{
	//pin setup
	DDRD &= ~(1 << DDD2); //zet PD2 als input
	PORTD |= (1 << PORTD2); //schakel pull-up aan voor PD2

	DDRB |= (1 << DDB5); //test

	//INT0 interrupt setup
	EICRA |= (1 << ISC00); //interrupt on any change INT0
	EIMSK |= (1 << INT0); //interrupt request enable INT0
	sei(); //global interrupt enable

	//i2c setup
	Wire.begin();

	//timer setup
	TCCR1B |= (1 << WGM12); //CTC modus aan
	OCR1A = OUTPUTCOMPAREMAX; //output compare waarde
	TIMSK1 |= (1 << OCIE1A); //output compare intterupts aan
	//De timer staat hier nog niet aan door geen prescaler in te stellen

	//zet het 7-segment display op 0
	show_time(ZERO);

	while(1) //loop
	{
		if (show_time_flag) //controleer of de show_timer_flag aan staat en roep show_timer() aan buiten de ISR
		{
			show_time(counter); //call functie om time weer te geven
			counter = ZERO; //reset de counter
			show_time_flag = FALSE; //zet de flag weer uit
		}
	}
}

void show_time(uint8_t time) //functie om het 7-segement display aan te sturen
{
	Wire.beginTransmission(0x20); //open serieel
	Wire.write(display[time]); //waarde om te sturen
	Wire.endTransmission(0x20); //sluit serieel en verstuur
}

ISR(INT0_vect) //interrupt van drukknop
{
	if (~(PIND | ~(1 << PIND2))) //knop ingedrukt
	{
		TCNT1 = ZERO; //reset timer naar 0
		TCCR1B |= (1 << CS12); //start timer
	}
	else //knop losgelaten
	{
		TCCR1B &= ~(1 << CS12); //stop timer
		show_time_flag = TRUE; //zet de flag aan om time te laten zien
	}
}

ISR(TIMER1_COMPA_vect) //interrupt van timer
{
	PORTB ^= (1 << PORTB5);	//toggle LED op de Arduino

	counter++; //increment counter
	if (counter > COUNTERMAX) //controleer voor overflow
	{
		counter = ZERO; //reset counter naar 0
	}
}
