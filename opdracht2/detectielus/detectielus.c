#include <avr/io.h>
#include <util/delay.h>

//functies
void display_counter(uint8_t counter);
enum bstate button_state();
void counter_add();

//variabelen
enum bstate { pressed, released };
uint8_t counter = 0;
#define MAX 15

int main (void)
{
	//pin setup
	//pin 0 als pull-up
	DDRD &= ~(1 << DDD0);
	PORTD |= (1 << PORTD0);

	//pin A0 t/m A3 als output pins
	DDRC |= (1 << DDC0) | (1 << DDC1) | (1 << DDC2) | (1 << DDC3);

	//loop
	while (1)
	{

		//laat de counter zien
		display_counter(counter);

		//controleer of de pin LOW gaat. deze is standaard HIGH.
		if (~(PIND | ~(1 << PIND0)))
		{
			counter_add();
		}
	}

	return(0);
}


void display_counter(uint8_t counter)
{
	PORTC = counter;
}

enum bstate button_state()
{
	//maak variable state voor gebruik in deze functie
	int state;
	if (PIND & (1 <<PIND0))
	{
		state = released;
	}
	else
	{
		state = pressed;
	}

	return state;
}

void counter_add()
{
	//wacht 20ms (debounce)
	_delay_ms(20);

	if (button_state() == released)
	{
		//tel 1 op bij de counter
		counter++;

		//controleer of er een overflow is
		if (counter > MAX)
		{
			counter = 0;
		}
	}
}
