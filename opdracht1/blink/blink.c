#include <avr/io.h>
#include <util/delay.h>

int main (void)
{
	// pin 30 output mode
	DDRB |= (1 << DDB5);

	while (1)
	{
		//toggle
		PORTB ^= (1 << PORTB5);
		_delay_ms(1000);
	}

	return(0);
}
